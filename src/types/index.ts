export type HeadersType = {
  dataIndex: string,
  title: string,
  width: number,
  sorter: boolean,
};
export type DateType = {
  [key: string | number]: string | number
}