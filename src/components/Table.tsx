import React, { useMemo, useState } from "react";
import "../App.css"
import { DateType, HeadersType } from "../types";
import InfiniteScroll from "react-infinite-scroll-component";

interface Props {
  headers: HeadersType[];
  data?: DateType[];
  onFilter: (mode: string) => void;
  onRemoveItems: (filtered: boolean) => void;
  onScroll: () => void;
  customShift: (index: number) => void;
}

const MainTable: React.FC<Props> = ({ customShift, onRemoveItems, headers, data, onFilter, onScroll }) => {
  const [sorting, setSorting] = useState<boolean>(false);
  const [inputName, setInputName] = useState<boolean>(false);
  const [inputRate, setInputRate] = useState<boolean>(false);
  const [selectedAll, setSelectedAll] = useState<boolean>(false);
  const [checkedRender, setCheckedRender] = useState<boolean>(false);
  const [inputNameValue, setInputNameValue] = useState<string>("");
  const [inputRateValue, setInputRateValue] = useState<string | number>("");

  const nameInput = useMemo(() => {
    return (
      <td>
        <input onChange={(e) => setInputNameValue(e.target.value)} type="text" />
        <button className="buttonInput" onClick={() => {
          setInputName(false);
        }}>Add</button>
      </td>
    )
  }, [])

  const rateInput = useMemo(() => {
    return (
      <td>
        <input onChange={(e) => setInputRateValue(e.target.value)} type="text" />
        <button className="buttonInput" onClick={() => {
          setInputRate(false);
        }}>Add</button>
      </td>
    )
  }, []);

  const scrollableElems = useMemo(() => {
    return (
    <div style={{ height: "150px", overflowY: "scroll" }} id="ref">
    <InfiniteScroll
      initialScrollY={57}
      dataLength={data?.length as number}
      next={() => onScroll()}
      hasMore={true}
      loader={<h4> </h4>}
      scrollableTarget="ref"
    >
      {data?.map((item, index) => {
        return (
          <tr className="scrollsTr" key={index}>
            {headers.map((header, index2) => (
              <td className={selectedAll ? "blueSelection" : ""} key={index2}>{item[header.dataIndex]}</td>
            ))}
            <td onClick={() => {
              customShift(index);
              setCheckedRender((prev) => !prev);
            }} className="rowButton">
              Delete Row
            </td>
          </tr>
        )
      })
      }
    </InfiniteScroll>
  </div>)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customShift, data, headers, onScroll, selectedAll, checkedRender])

  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
      <table>
        <tbody>
          <tr>
            {headers.map((item, index) => {
              return (
                <td
                  style={{ width: `${item.width}px` }}
                  className="headTr"
                  key={index}
                >
                  {item.title}
                  <span onClick={() => {
                    data?.reverse();
                    setSorting((prev) => !prev);
                    onFilter((sorting && "asc") || "desc");
                  }} className="">
                    {item.sorter && "^"}
                  </span>
                </td>
              )
            }
            )}
          </tr>
          {scrollableElems}
          <tr className="addingTD">
            {(inputName && (
              nameInput
            )) || (
                <td onClick={() => setInputName(true)}><span style={{ cursor: "pointer" }}>Add Name</span></td>
              )}
            {(inputRate && (
              rateInput
            )) || (
                <td onClick={() => setInputRate(true)}><span style={{ cursor: "pointer" }}>Add Rating</span></td>
              )}
            <td className="selectingElems">
              <span onClick={() => setSelectedAll((prev) => !prev)}>
                {selectedAll ? "Deselect All" : "Selected All"}
              </span>
            </td>
            {selectedAll && (
              <td onClick={() => {
                onRemoveItems(selectedAll);
                setSelectedAll(false);
              }} className="allDeleting"><span>Delete All</span></td>
            )}
          </tr>
          <tr className="createElem"><td onClick={() => {
            setCheckedRender((prev) => !prev);
            data?.push({ name: inputNameValue, rate: inputRateValue })
          }}>Create</td></tr>
        </tbody>
      </table>
    </div>
  );
};

export default MainTable;