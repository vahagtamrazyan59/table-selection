import React, { useCallback, useEffect, useState } from "react";
import { DateType, HeadersType } from "../types";
import MainTable from "./Table";
import 'react-virtualized/styles.css';

const MainLayout = () => {
  const [filteringTable, setFilteringTable] = useState<string>("asc");
  const [deleteAll, setDeleteAll] = useState<boolean>(false);
  const [data, setData] = useState<DateType[]>([]);
  const [headers, setHeaders] = useState<HeadersType[]>([]);

  useEffect(() => {
    fetch('data.json')
      .then(response => response.json())
      .then((res: {data: DateType[], headers: HeadersType[]}) => {
        setData(res.data);
        setHeaders(res.headers)
      })
      .catch(console.warn)
  },[])

  const onScroll = () => {
    setData(data.concat(data));
  }

  const customShift = useCallback((index) => {
    data.splice(index, 1)
    setData(data);
  },[data])

  const sortingToggle = useCallback(() => {
    if (deleteAll) {
      for (let i = 0; i <= data.length + 1; i++) {
        data.pop();
      }
    }
    if (filteringTable === "asc") return data;
    if (filteringTable === "desc") {
      return data.reverse();
    }
  }, [data, deleteAll, filteringTable])
  const onFilter = useCallback((mode: string) => {
    setFilteringTable(mode);
  }, [])

  const onRemoveItems = useCallback((filtered: boolean) => {
    setDeleteAll(filtered);
  }, [])

  return (
      <MainTable
        customShift={customShift}
        onScroll={onScroll}
        onRemoveItems={onRemoveItems}
        onFilter={onFilter}
        headers={headers}
        data={sortingToggle()}
      />
  )
};

export default MainLayout;